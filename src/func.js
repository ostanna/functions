const getSum = (str1, str2) => {
  if (typeof(str1) != "string" || typeof(str2) != "string")
    return false;
  let res="";
  if (str1.length==0)
    return str2;
  if (str2.length==0)
    return str1;
  for (let i=0; i<str2.length && i<str1.length; i++)
  {
    let n1=Number(str1[i]);
    let n2=Number(str2[i]);
    if (Number.isNaN(n1) || Number.isNaN(n2))
      return false;
    res+=(n1+n2).toString();
  }
  return res;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postsCount=0;
  let commentsCount=0;
  for (let post of listOfPosts)
  {
    if (post.author==authorName)
      postsCount++;
    if (post.comments)  
      for (let comm of post.comments)
        if (comm.author==authorName)
          commentsCount++;
  }
  return 'Post:'+postsCount+',comments:'+commentsCount;
};

const tickets=(people)=> {
  let twf=0;
  let fty=0;
  for (let person of people)
  {
      if (person==25)
        twf+=1;
      else if (person==50)
      {
        if (twf<1)
          return "NO";
        twf--;
        fty++;
      }
      else//100
      {
        if (twf<1)
          return "NO";
        if (fty>0)
        {
          fty--;
          twf--;
        }
        else if (twf>2)
        {
          twf-=3;
        }
        else
          return "NO";
      }
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
